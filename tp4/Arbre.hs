module Arbre where 

import Control.Concurrent (threadDelay)
import Test.QuickCheck

-- Q1
data Arbre couleur valeur = Feuille 
    | Noeud (Arbre couleur valeur) couleur valeur (Arbre couleur valeur) 
    deriving (Show)

data Sorel = R | N
    deriving (Eq, Show)

-- Q2
map' :: (v -> v) -> Arbre c v -> Arbre c v
map' _ Feuille = Feuille
map' f (Noeud gauche couleur valeur droite) = Noeud (map' f gauche) couleur (f valeur) (map' f droite)

foldArbre :: (c -> v -> acc -> acc -> acc) -> acc -> Arbre c v -> acc
foldArbre _ acc Feuille = acc
foldArbre f acc (Noeud gauche couleur valeur droite) = f couleur valeur (foldArbre f acc gauche) (foldArbre f acc droite)

-- Q3
taille :: (Num n) => Arbre c v -> n
taille = foldArbre (\_ _ g d -> 1 + g + d) 0

taille' :: (Num n) => Arbre c v -> n
taille' Feuille = 0
taille' (Noeud g _ _ d) = 1 + taille g + taille d

hauteur :: (Num n, Ord n) => Arbre c v -> n
hauteur = foldArbre (\_ _ g d -> 1 + max g d) 0

hauteur' :: (Num n, Ord n) => Arbre c v -> n
hauteur' Feuille = 0
hauteur' (Noeud gauche _ _ droite) = 1 + max (hauteur' gauche) (hauteur' droite)

-- Q4
peigneGauche :: [(c, v)] -> Arbre c v
peigneGauche [] = Feuille
peigneGauche ((c, v):xs) = Noeud (peigneGauche xs) c v Feuille

-- Q5
-- Cette fonction vérifie que la liste passée à la fonction peigneGauche fait la même longueur que l'arbre généré par la fonction.
-- On sera donc surs que l'arbre résultant fait bien la bonne longueur
prop_hauteurPeigne :: [(c, v)] -> Bool
prop_hauteurPeigne xs = length xs == hauteur (peigneGauche xs)

-- Q6

-- Q7
estComplet' :: Arbre c v -> Bool
estComplet' Feuille = True
estComplet' (Noeud gauche _ _ droite) = hauteur gauche == hauteur droite && estComplet' gauche && estComplet' droite

-- Q8 
-- Il n'y a que les arbres complets de hauteur 0 ou 1 qui sont des peignes gauches

prop_peigneComplet :: [(c, v)] -> Bool
prop_peigneComplet xs = not (estComplet' $ peigneGauche xs)

-- Q9

complet :: Int -> [(c, v)] -> Arbre c v
complet 0 _ = Feuille
complet n xs = Noeud (complet (n - 1) gauche) c v (complet (n - 1) droite)
    where   (c, v) = xs !! milieu
            milieu = (2^n - 1) `div` 2
            newLists = splitAt milieu xs
            gauche = fst newLists
            droite = (tail . snd) newLists

prop_complet :: Int -> Bool
prop_complet h
    | h > 10 = True
    | otherwise = estComplet' (complet h ys) 
    where   ys = take nb (iterate id ((), 42))
            nb = 2 ^ h - 1

-- Q10
-- Cette fonction est la fonction repeat

repeat' :: a -> [a]
repeat' = iterate id

-- Q11
tuplify :: [a] -> [((), a)]
tuplify [] = []
tuplify (x:xs) = ((), x) : (tuplify xs)

simpleTuplesList :: [((), Char)]
simpleTuplesList = tuplify ['a'..]

-- Q12
aplatit :: Arbre c v -> [(c, v)]
aplatit Feuille = []
aplatit (Noeud gauche c v droite) = aplatit gauche ++ [(c, v)] ++ aplatit droite

complet4 :: Arbre () Char 
complet4 = complet 4 xs
    where xs = take 15 simpleTuplesList

prop_aplatit :: Bool
prop_aplatit = map snd (aplatit complet4) == "abcdefghijklmno"

-- Q13
element :: Eq v => v -> Arbre c v -> Bool
element e arbre = e `elem` liste
    where liste = map snd $ aplatit arbre

-- Q14
noeud :: (c -> String) -> (v -> String) -> (c, v) -> String
noeud f g (c, v) = (g v) ++ " [color=" ++ color ++ ", fontcolor=" ++ color ++ "]"
    where color = (f c)

-- Q15
arcs :: Arbre c v -> [(v,v)]
arcs Feuille = []
arcs (Noeud gauche _ v droite) = (getTuple v gauche) ++ (getTuple v droite) ++ (arcs gauche) ++ (arcs droite)
    where getTuple _ Feuille = []
          getTuple val (Noeud _ _ fv _) = [(val, fv)]

-- Q16
arc :: (v -> String) -> (v, v) -> String
arc f (a, b) = (f a) ++ " -> " ++ (f b)

-- Q17
dotise :: String -> (c -> String) -> (v -> String) -> Arbre c v -> String
dotise name f g arbre = entete ++ entete2 ++ (unlines nodes) ++ (unlines (textArcs (arcs arbre))) ++ end
    where   entete = "digraph \"" ++ name ++ "\" {"
            entete2 = "node [fontname=\"DejaVu-Sans\", shape=circle]"
            nodes = [] : getValues (aplatit arbre)
            getValues [] = []
            getValues (x:xs) = (noeud f g x) : getValues xs
            textArcs [] = []
            textArcs (y:ys) = (arc g y) : textArcs ys
            end = "}"

-- Q18.
elementR :: Ord v => v -> Arbre c v -> Bool
elementR _ Feuille = False
elementR val (Noeud g _ v d) 
        | val == v  = True
        | val <  v  = elementR val g
        | otherwise = elementR val d

-- Q20.
equilibre :: (Ord a) => Arbre Sorel a -> Arbre Sorel a
equilibre (Noeud (Noeud (Noeud a R x b) R y c) N z d) = Noeud (Noeud a N x b) R y (Noeud c N z d)
equilibre (Noeud (Noeud a R x (Noeud b R y c)) N z d) = Noeud (Noeud a N x b) R y (Noeud c N z d) 
equilibre (Noeud a N x (Noeud (Noeud b R y c) R z d)) = Noeud (Noeud a N x b) R y (Noeud c N z d)  
equilibre (Noeud a N x (Noeud b R y (Noeud c R z d))) = Noeud (Noeud a N x b) R y (Noeud c N z d)
equilibre a = a

-- Q21.
insert' :: (Ord a) => a -> Arbre Sorel a -> Arbre Sorel a
insert' nv arbre@(Noeud g c v d) 
    | nv == v   = arbre
    | nv <  v   = equilibre (Noeud (insert' nv g) c v d)
    | otherwise = equilibre (Noeud g c v (insert' nv d))
insert' v Feuille = Noeud Feuille R v Feuille

insert :: (Ord a) => a -> Arbre Sorel a -> Arbre Sorel a
insert nv arbre = racine_noire (insert' nv arbre)

racine_noire :: Arbre Sorel a -> Arbre Sorel a
racine_noire (Noeud g _ v d) = (Noeud g N v d)
racine_noire Feuille = Feuille

-- Q22
prop_valeurs :: (Ord v) => Arbre Sorel v -> Bool
prop_valeurs Feuille = True
prop_valeurs (Noeud g _ v d) = superieur v g && inferieur v d
    where   superieur _ Feuille = True
            superieur val (Noeud gauche _ vf droite) = val > vf && superieur val gauche && superieur val droite
            inferieur _ Feuille = True
            inferieur val (Noeud gauche _ vf droite) = val < vf && inferieur val gauche && inferieur val droite

prop_altern_couleur :: Arbre Sorel v -> Bool
prop_altern_couleur Feuille = True
prop_altern_couleur (Noeud g c _ d) = estDiff c g && estDiff c d
    where estDiff _ Feuille = True
          estDiff coul (Noeud gauche cf _ droite) = (coul == N || cf == N) && estDiff cf gauche && estDiff cf droite

-- Q23
arbresDot :: (Show a, Ord a) => [a] -> [String]
arbresDot [] = []
arbresDot xs = arbresDot' xs Feuille

arbresDot' :: (Show a, Ord a) => [a] -> Arbre Sorel a -> [String]
arbresDot' [] _ = []
arbresDot' (x:xs) arbre = (dotise "arbre" sorelToStr valToStr arbre') : arbresDot' xs arbre'
    where arbre' = insert x arbre 

main :: IO ()
main = mapM_ ecrit arbres
    where ecrit a = do writeFile "arbre.dot" a
                       threadDelay 100000
          arbres = arbresDot ['a'..'z']
          -- arbres = arbresDot "gcfxieqzrujlmdoywnbakhpvst"

sorelToStr :: Sorel -> String
sorelToStr R = "red"
sorelToStr N = "black"

valToStr :: (Show v) => v -> String
valToStr v = ((show v) !! 1) : []
