import Parser
import Control.Monad
import Control.Applicative
import System.IO

type Nom = String

data Expression = Lam Nom Expression
                | App Expression Expression
                | Var Nom
                | Lit Litteral
                deriving (Show,Eq)

data Litteral = Entier Integer
              | Bool   Bool
              deriving (Eq)

data ValeurA = VLitteralA Litteral
             | VFonctionA (ValeurA -> ValeurA)

type Environnement a = [(Nom, a)]

-- Q1
espacesP :: Parser ()
espacesP = do zeroOuPlus $ car ' '
              return ()

-- Q2
nomP :: Parser Nom
nomP =  do  r <- unOuPlus (carCond estLettre)
            espacesP 
            return r

estLettre :: Char -> Bool
estLettre c = c `elem` ['a'..'z']

-- Q3
varP :: Parser Expression
varP = do   r <- nomP
            return (Var r)

-- Q4
applique :: [Expression] -> Expression
applique [e] = e
applique [e1, e2] = App e1 e2
applique es = App (applique (init es)) (last es)

-- Q5
exprP :: Parser Expression
exprP = exprParentheseeP ||| lambdaP ||| varP ||| nombreP ||| booleenP

exprsP :: Parser Expression
exprsP = do     rs <- unOuPlus exprP
                return (applique rs)

-- Q6
lambdaP :: Parser Expression
lambdaP = do    car '\\'
                espacesP
                vs <- nomP
                espacesP
                car '-'
                car '>'
                espacesP
                ex <- exprsP
                espacesP
                return (Lam vs ex)

-- Q8
exprParentheseeP :: Parser Expression
exprParentheseeP = do   car '('
                        ex <- exprsP
                        car ')'
                        espacesP
                        return ex

-- Q9
nombreP :: Parser Expression
nombreP = do    nb <- unOuPlus $ carCond estNombre
                espacesP
                return (Lit (Entier (read nb)))

estNombre :: Char -> Bool
estNombre c = c `elem` ['0'..'9']

-- Q10
booleenP :: Parser Expression
booleenP = do   bool <- chaine "True" ||| chaine "False"
                espacesP
                return (Lit (Bool (read bool)))

-- Q11
expressionP :: Parser Expression
expressionP = do    espacesP
                    exprsP

-- Q12
ras :: String -> Expression
ras str  
    | complet result = resultat result
    | result == Nothing = error "Rien n'a été parsé"
    | otherwise = error "Erreur d'analyse syntaxique"
    where result = runParser expressionP str

-- Q13
-- Haskell n'est pas capable de générer un affichage pour les fonctions ValeurA->ValeurA, il nous
-- suggère de définir une fonction show pour pouvoir l'afficher.

-- Q14
instance Show ValeurA where
    show (VFonctionA _) = "λ"
    show (VLitteralA l) = show l

instance Show Litteral where
    show (Entier i) = show i
    show (Bool b) = show b

-- Q15
interpreteA :: Environnement ValeurA -> Expression -> ValeurA
interpreteA env (Lam nom exp1) = (VFonctionA f)
    where   f = (\var -> interpreteA ((nom, var):env) exp1)
interpreteA env (App exp1 exp2) = appl e1 (interpreteA env exp2)
    where   e1 = interpreteA env exp1
            appl (VFonctionA f) a = f a
            appl _ _ = error "l'élément appliqué n'est pas une fonction"
interpreteA env (Var nom) = res $ lookup nom env
    where   res (Just(a)) = a
            res Nothing = error "Variable introuvable dans l'environnement"
interpreteA _ (Lit lit) = VLitteralA lit

-- Q16
negA :: ValeurA
negA = (VFonctionA f)
    where   f (VLitteralA (Entier i)) = VLitteralA (Entier (-1 * i))

-- Q17
addA :: ValeurA
addA = (VFonctionA (\(VLitteralA (Entier a)) -> VFonctionA (\(VLitteralA (Entier b)) -> (VLitteralA (Entier (a + b))))))

-- Q18
envA :: Environnement ValeurA
envA = [("neg",   negA)
        , ("add",   releveBinOpEntierA (+))
        , ("soust", releveBinOpEntierA (-))
        , ("mult",  releveBinOpEntierA (*))
        , ("quot",  releveBinOpEntierA quot)]

releveBinOpEntierA :: (Integer -> Integer -> Integer) -> ValeurA
releveBinOpEntierA f = (VFonctionA (\(VLitteralA (Entier a)) -> VFonctionA (\(VLitteralA (Entier b)) -> (VLitteralA (Entier (a `f` b))))))

-- Q19
ifthenelseA :: ValeurA
ifthenelseA = (VFonctionA (\(VLitteralA (Bool bool)) -> (VFonctionA (\(VLitteralA (Entier a)) -> (VFonctionA (\(VLitteralA (Entier b)) -> 
    if bool then (VLitteralA (Entier a)) else (VLitteralA (Entier b))))))))

-- Q20

main :: IO ()
main = do   putStr "minilang>"
            hFlush stdout
            str <- getLine
            putStrLn $ show (interpreteA envA (ras str))
            main
-- Q21
data ValeurB = VLitteralB Litteral
             | VFonctionB (ValeurB -> ErrValB)

type MsgErreur = String
type ErrValB   = Either MsgErreur ValeurB

instance Show ValeurB where
    show (VFonctionB _) = "λ"
    show (VLitteralB l) = show l

-- Q22
interpreteB :: Environnement ValeurB -> Expression -> ErrValB
interpreteB env (Lam nom exp1) = (Right (VFonctionB f))
    where   f = (\var -> interpreteB ((nom, var):env) exp1)
interpreteB env (App exp1 exp2) = errorCheck e1 e2
    where   e1 = interpreteB env exp1
            e2 = interpreteB env exp2
            errorCheck (Right (VLitteralB l)) _ = (Left ((show l) ++ " n'est pas une fonction. Application impossible."))
            errorCheck error@(Left _) _ = error
            errorCheck _ error@(Left _) = error
            errorCheck (Right (VFonctionB f)) (Right val) = applyFunction (f val)
            applyFunction res@(Right val) = res
            applyFunction error = error
interpreteB env (Var nom) = process $ lookup nom env
    where   process (Just a) = (Right a)
            process _ = (Left ("La variable " ++ (show nom) ++ " n'est pas définie."))
interpreteB _ (Lit lit) = Right (VLitteralB lit)

-- Q23
addB :: ValeurB
addB = VFonctionB f
    where   f (VLitteralB (Entier e1)) = Right (VFonctionB g)
                where   g (VLitteralB (Entier e2)) = Right (VLitteralB (Entier (e1 + e2)))
                        g (VLitteralB (Bool b)) = Left (show b ++ " n'est pas un entier")
                        g (VFonctionB _) = Left ("λ n'est pas un entier")

            f (VLitteralB (Bool b)) = Left (show b ++ " n'est pas un entier")
            f (VFonctionB _) = Left ("λ n'est pas un entier")
            

-- Q24
quotB :: ValeurB
quotB = VFonctionB f
    where   f (VLitteralB (Entier e1)) = Right (VFonctionB g)
                where   g (VLitteralB (Entier 0)) = Left "Division par zéro"
                        g (VLitteralB (Entier e2)) = Right (VLitteralB (Entier (quot e1 e2)))
                        g (VLitteralB (Bool b)) = Left (show b ++ " n'est pas un entier")
                        g (VFonctionB _) = Left ("λ n'est pas un entier")
            f (VLitteralB (Bool b)) = Left (show b ++ " n'est pas un entier")
            f (VFonctionB _) = Left ("λ n'est pas un entier")


data ValeurC = VLitteralC Litteral
             | VFonctionC (ValeurC -> OutValC)

type Trace   = String
type OutValC = (Trace, ValeurC)

-- Q25
instance Show ValeurC where
    show (VFonctionC _) = "λ"
    show (VLitteralC l) = show l

-- Q26
interpreteC :: Environnement ValeurC -> Expression -> OutValC
interpreteC env (Lam nom exp1) = ("", VFonctionC (\var -> interpreteC ((nom, var):env) exp1))
interpreteC env (App exp1 exp2) = (t1 ++ t2 ++ "." ++ t3, val3)
    where   (t1, (VFonctionC f))  = interpreteC env exp1
            (t2, val2) = interpreteC env exp2
            (t3, val3) = f val2
            getOutVal str1 str2 (str3, (VLitteralC (lit))) = (str1 ++ str2 ++ str3, (VLitteralC lit))
interpreteC env (Var nom) = process $ lookup nom env
    where   process (Just a) = ("", a)
            process _ = error "Variable absente de l'environnement"
interpreteC _ (Lit lit) = ("", (VLitteralC lit))

-- Q27
pingC :: ValeurC
pingC = VFonctionC(f)
    where f val = ("p", val)

-- Q28
data ValeurM m = VLitteralM Litteral
               | VFonctionM (ValeurM m -> m (ValeurM m))

instance Show (ValeurM m) where
    show (VLitteralM l) = show l
    show (VFonctionM _) = "λ"

-- Q29
data SimpleM v = S v
               deriving Show

interpreteSimpleM :: Environnement (ValeurM SimpleM) -> Expression -> SimpleM (ValeurM SimpleM)
interpreteSimpleM env (Lam nom exp1) = S (VFonctionM f)
    where   f = (\var -> interpreteSimpleM ((nom, var):env) exp1)
interpreteSimpleM env (App exp1 exp2) = appl e1 e2
    where   e1 = interpreteSimpleM env exp1
            e2 = interpreteSimpleM env exp2
            appl (S (VFonctionM f)) (S a) = f a
            appl _ _ = error "L'élément appliqué n'est pas une fonction"
interpreteSimpleM env (Var nom) = S (res $ lookup nom env)
    where   res (Just(a)) = a
            res Nothing = error "Variable introuvable dans l'environnement"
interpreteSimpleM _ (Lit lit) = S (VLitteralM lit)

instance Monad SimpleM where
    return      = S
    (S v) >>= f = f v

instance Applicative SimpleM where
    pure  = return
    (<*>) = ap

instance Functor SimpleM where
    fmap  = liftM

-- Q30
interpreteM :: Monad m => Environnement (ValeurM m) -> Expression -> m (ValeurM m)
interpreteM _ (Lit lit) = return (VLitteralM lit)
interpreteM env (Var nom) = return (res $ lookup nom env)
    where   res (Just a) = a
            res Nothing = error "Variable introuvable dans l'environnement"
interpreteM env (App exp1 exp2) = (interpreteM env exp1) >>= step1
    where   step1 (VFonctionM f) = (interpreteM env exp2) >>= f
interpreteM env (Lam nom exp1) = return (VFonctionM f)
    where   f = (\var -> interpreteM ((nom, var):env) exp1)

type InterpreteM m = Environnement (ValeurM m) -> Expression -> m (ValeurM m)

interpreteS :: InterpreteM SimpleM
interpreteS = interpreteM

-- Q31
{-
*Main> interpreteS [] (ras "42")
S 42
*Main> interpreteSimpleM [] (ras "42")
S 42
*Main> interpreteS [] (ras "(\\x -> x) True")
S True
*Main> interpreteSimpleM [] (ras "(\\x -> x) True")
S True
-}

-- Q32
data TraceM v = T (Trace, v)
              deriving Show

instance Monad TraceM where
    return v = T ("", v)
    T (t1, v1) >>= f = (appl (f v1))
        where appl (T (t2, v2)) = T (t1 ++ t2, v2)

instance Applicative TraceM where
    pure  = return
    (<*>) = ap

instance Functor TraceM where
    fmap  = liftM            

interpreteMT :: InterpreteM TraceM
interpreteMT = interpreteM

pingM :: ValeurM TraceM
pingM = VFonctionM (\v -> T ("p", v))

-- Q33
interpreteMT' :: InterpreteM TraceM
interpreteMT' env (App exp1 exp2) = T (t1 ++ t2 ++ "." ++ t3, v3)
    where   T (t1, (VFonctionM f)) = interpreteMT' env exp1
            T (t2, v2) = interpreteMT' env exp2
            T (t3, v3) = f v2
interpreteMT' env (Lam nom exp1) = return (VFonctionM f)
    where   f = (\var -> interpreteMT' ((nom, var):env) exp1)
interpreteMT' env v = interpreteMT env v

-- Q34
data ErreurM v = Succes v
               | Erreur String
               deriving Show

instance Monad ErreurM where
    fail e = Erreur e
    return s = Succes s
    Succes v >>= f = f v
    Erreur e >>= _ = fail e

instance Applicative ErreurM where
    pure  = return
    (<*>) = ap

instance Functor ErreurM where
    fmap  = liftM

-- Q35
interpreteE :: InterpreteM ErreurM
interpreteE env (Var nom) = res $ lookup nom env
    where   res (Just a) = return a
            res Nothing = fail ("La variable " ++ nom ++ " n'est pas définie")
interpreteE env (App exp1 exp2) = (interpreteE env exp1) >>= step1
    where   step1 (VFonctionM f) = (interpreteE env exp2) >>= f
            step1 (VLitteralM lit) = fail (show lit ++ " n'est pas une fonction, application impossible.")
interpreteE env (Lam nom exp1) = return (VFonctionM f)
    where   f = (\var -> interpreteE ((nom, var):env) exp1)
interpreteE env lit = interpreteM env lit
