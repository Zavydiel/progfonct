main = undefined

-- Q3
sommeDeXaY :: Int -> Int -> Int
sommeDeXaY x y = sum [x..y]

-- Q4
somme :: (Num a) => [a] -> a
somme [] = 0
somme (x:xs) = x + somme xs

-- Q5
last' :: [a] -> a
last' [] = error "No last element in empty list"
last' [x] = x
last' (x:xs) = last' xs

init' :: [a] -> [a]
init' ys@(_:xs) = take (length xs) ys

-- Q6
(!!!) :: [a] -> Int -> a
(!!!) [] _ = error "Can't get list element in empty list"
(!!!) (x:_) 0 = x
(!!!) (_:xs) n = xs !!! (n - 1)

(+++) :: [a] -> [a] -> [a]
(+++) [] ys = ys
(+++) (x:xs) ys = x : xs +++ ys

concat' :: [[a]] -> [a]
concat' [] = []
concat' (xs:xss) = xs +++ concat' xss

map' :: (a -> b) -> [a] -> [b]
map' _ [] = []
map' f (x:xs) = f x : map' f xs

-- Q7
-- x est une fonction qui prend en paramètre un entier n et renvoie le nième élément de 1

-- Q8
longueur :: [a] -> Int
longueur xs = somme (map' (\_ -> 1) xs)

-- Q9
powermap :: (a -> a) -> a -> Int -> [a]
powermap _ _ n
    | n < 0 = error "Can't work with n < 0"
powermap f x 0 = [x]
powermap f x n = x : powermap f (f x) (n-1)

powermap' :: (a -> a) -> a -> Int -> [a]
powermap' _ _ n
    | n < 0 = error "Can't work with n < 0"
powermap' f x n = take n (iterate f x)

-- Q10
listentiers :: Int -> [Int]
listentiers n = powermap inc 0 n
    where inc x = x + 1

