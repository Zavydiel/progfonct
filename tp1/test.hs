-- Q3.
sommeDeXaY :: Int -> Int -> Int
sommeDeXaY x y = sum [x..y]

-- Q4.
somme :: Num a => [a] -> a
somme xs = case xs of
        []      -> 0
        x:xs' -> x + somme xs'

-- Q5.
mlast :: [a] -> a
mlast xs = head (reverse xs)

minit :: [a] -> [a]
minit xs = take (length xs - 1) xs

-- Q6.
(!!!) :: [a] -> Int -> a
(!!!) [] _ = error "oob"
(!!!) (x:_) 0 = x
(!!!) (_:xs) n = xs !!! (n - 1)

(+++) :: [a] -> [a] -> [a]
(+++) [] ys = ys
(+++) (x:xs) ys = x: xs +++ ys

concatenate :: [[a]] -> [a]
concatenate [] = []
concatenate (xs:xss) = xs +++ concatenate xss

mmap :: (a -> b) -> [a] -> [b]
mmap _ [] = []
mmap f (x:xs) = f x : mmap f xs

-- Q7.
-- x est une fonction qui prend un paramètre entier n et renvoie le nième élément de l

-- Q8.
one :: a -> Int
one _ = 1

longueur :: [a] -> Int
longueur [] = 0
longueur xs = somme (map one xs)
