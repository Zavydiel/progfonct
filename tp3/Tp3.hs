module Tp3 where

import Graphics.Gloss
import Debug.Trace
type Symbole  = Char
type Mot      = [Symbole]
type Axiome   = Mot
type Regles   = Symbole -> Mot
type LSysteme = [Mot]
type EtatTortue = (Point, Float)

type EtatDessin = ([EtatTortue], [Path])

type Config = (EtatTortue -- État initial de la tortue
            ,Float      -- Longueur initiale d’un pas
            ,Float      -- Facteur d’échelle
            ,Float      -- Angle pour les rotations de la tortue
            ,[Symbole]) -- Liste des symboles compris par la tortue

tRegle :: Symbole -> Mot
tRegle symb = [symb, symb]

motSuivant'' :: Regles -> Mot -> Mot
motSuivant'' regle mot = foldr (\symbole acc -> regle symbole ++ acc) [] mot

motSuivant' :: Regles -> Mot -> Mot
motSuivant' regle mot = concat [(regle symb) | symb <- mot]

motSuivant :: Regles -> Mot -> Mot
motSuivant _ [] = []
motSuivant regle (m:ms) = regle m ++ motSuivant regle ms

regleKoch :: Symbole -> Mot
regleKoch 'F' = "F-F++F-F"
regleKoch '+' = "+"
regleKoch '-' = "-"
regleKoch _ = error "No rule for thy Symbol"

lsysteme :: Axiome -> Regles -> LSysteme
lsysteme axiome regle = iterate (motSuivant' regle) axiome

systemeKoch :: LSysteme
systemeKoch = lsysteme "F" regleKoch

etatInitial :: Config -> EtatTortue
etatInitial (etat, _, _, _, _) = etat

longueurPas :: Config -> Float
longueurPas (_, len, _, _, _) = len

facteurEchelle :: Config -> Float
facteurEchelle (_, _, echelle, _, _) = echelle

angle :: Config -> Float
angle (_, _, _, ang, _) = ang

symbolesTortue :: Config -> [Symbole]
symbolesTortue (_, _, _, _, symboles) = symboles

avance :: Config -> EtatTortue -> EtatTortue
avance conf etat = prochainePosition etat (longueurPas conf) 

getX :: EtatTortue -> Float
getX etat = fst (fst etat)

getY :: EtatTortue -> Float
getY etat = snd (fst etat)

getCap :: EtatTortue -> Float
getCap etat = snd etat

getPos :: EtatTortue -> Point
getPos (p, _) = p

prochainePosition :: EtatTortue -> Float -> EtatTortue
prochainePosition etat len = ((x', y'), cap)
    where cap = getCap etat
          x' = getX etat + len * cos cap
          y' = getY etat + len * sin cap

tourneAGauche :: Config -> EtatTortue -> EtatTortue
tourneAGauche conf (p, a) = (p, a + (angle conf))  

tourneADroite :: Config -> EtatTortue -> EtatTortue
tourneADroite conf (p, a) = (p, a - (angle conf))

filtreSymbolesTortue :: Config -> Mot -> Mot
filtreSymbolesTortue _ [] = []
filtreSymbolesTortue conf (c:mot) 
    | c `elem` (symbolesTortue conf) = c : filtreSymbolesTortue conf mot
    | otherwise = filtreSymbolesTortue conf mot

sauvegardeEtatCourant :: EtatDessin -> EtatDessin
sauvegardeEtatCourant ([], _) = error "Nothing to save"
sauvegardeEtatCourant ((courant:reste), paths) = ((courant:courant:reste), paths)

chargeDernierEtat :: EtatDessin -> EtatDessin
chargeDernierEtat ([], _) = error "Nothing to load"
chargeDernierEtat ((courant:etats), paths) = (etats, [point]:paths)
    where point = getPos courant

interpreteSymbole :: Config -> EtatDessin -> Symbole -> EtatDessin
interpreteSymbole _ etat '[' = sauvegardeEtatCourant etat 
interpreteSymbole _ etat ']' = chargeDernierEtat etat
interpreteSymbole conf ((etat:etats), (path:paths)) sym = (nouvEtat:etats, (point:path):paths)
        where   nouvEtat = f conf etat
                point = getPos nouvEtat
                f = action sym
                action 'F' = avance
                action '+' = tourneAGauche
                action '-' = tourneADroite
                action _ = error "Unrecognized symbol"

interpreteMot :: Config -> Mot -> Picture
interpreteMot conf mot = line (concat path)
    where path = snd (foldl (\etat symb -> (interpreteSymbole conf etat symb)) etatDessinIni mot')
          mot' = filtreSymbolesTortue conf mot
          etatDessinIni = ([etatTortueIni], [[getPos etatTortueIni]])
          etatTortueIni = etatInitial conf

lsystemeAnime :: LSysteme -> Config -> Float -> Picture
lsystemeAnime lsys conf instant = interpreteMot conf' $ lsys !! enieme
    where enieme = round instant `mod` 6
          conf' = (etatInitial conf, newPas, echelle, angle conf, symbolesTortue conf)
          newPas = (longueurPas conf) * (echelle ^ enieme)
          echelle = facteurEchelle conf
