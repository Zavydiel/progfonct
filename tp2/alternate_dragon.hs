import Graphics.Gloss

main = animate (InWindow "Dragon" (500, 500) (0,0)) white (dragonAnime (50, 250) (450, 250))

dragonAnime :: Point -> Point -> Float -> Picture
dragonAnime a b t = Line (dragonOrdre a b (round t `mod` 20))

pointAIntercaler :: Point -> Point -> Point
pointAIntercaler (xa, ya) (xb, yb) = ((xa + xb)/2 + (yb - ya)/2,(ya + yb)/2 + (xa - xb)/2)

-- Q8
dragonOrdre :: Point -> Point -> Int -> Path
dragonOrdre a b 0 = a:b:[]
dragonOrdre a b 1 = a:pointAIntercaler a b: b: []
dragonOrdre a b n = dragonOrdre a c idx ++ reverse(dragonOrdre b c idx)
    where idx = (n - 1)
          c = pointAIntercaler a b

