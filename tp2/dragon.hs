import Graphics.Gloss

main = animate (InWindow "Dragon" (500, 500) (0,0)) white (dragonAnime (50, 250) (450, 250))

dragonAnime :: Point -> Point -> Float -> Picture
dragonAnime a b t = Line (dragon a b !! (round t `mod` 20))

-- Q5
pointAIntercaler :: Point -> Point -> Point
pointAIntercaler (xa, ya) (xb, yb) = ((xa + xb)/2 + (yb - ya)/2,(ya + yb)/2 + (xa - xb)/2)

-- Q6
pasDragon :: Path -> Path
pasDragon [] = error "at least one point needed"
pasDragon [x] = [x]
pasDragon [a, b] = a:pointAIntercaler a b:b:[]
pasDragon (a:b:c:xs) = a:pointAIntercaler a b: b: pointAIntercaler c b: (pasDragon (c:xs)) 

-- Q7
dragon :: Point -> Point -> [Path]
dragon p1 p2 = iterate pasDragon (p1:p2:[])

-- Q8
dragonOrdre :: Point -> Point -> Int -> Path
dragonOrdre a b 0 = a:b:[]
dragonOrdre a b 1 = a:pointAIntercaler a b: b: []
dragonOrdre a b n = dragonOrdre a c idx ++ reverse(dragonOrdre b c idx)
    where idx = (n - 1)
          c = pointAIntercaler a b

